package ru.am;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        try (Scanner scn = new Scanner(System.in)) {
            System.out.print("Введите температуру (целое число): ");
            int temperature = scn.nextInt();
            System.out.print("Введите скорость ветра: ");
            int wind = scn.nextInt();
            System.out.print("На улице идет дождь? Y/N: ");
            String answer = scn.next();
            boolean rain = answer.equalsIgnoreCase("y");

            if (temperature <= -10) {
                System.out.print("На улице очень холодно");
                if (wind >= 8) System.out.print(" и сильный ветер. Надевай все что есть в шкафу.");
                else System.out.println(". Оденься очень тепло.");
            } else if (temperature <= 0) {
                System.out.print("На улице холодно");
                if (wind >= 8) System.out.print(" и сильный ветер. Надевай зимнюю одежду.");
                else System.out.println(". Надень зимнюю куртку и ботинки.");
            } else if (temperature <= 10) {
                System.out.print("На улице прохладно ");
                if (wind >= 8) System.out.print(" и сильный ветер. Надень осеннюю куртку и шапку.");
                else System.out.println(". Надень осеннюю куртку.");
            } else if (temperature <= 20) {
                System.out.print("На улице тепло");
                if (wind >= 8) System.out.print(" и сильный ветер");
                System.out.println(". Надевай джинсовку/ветровку/худи.");
            } else {
                System.out.print("На улице жарко");
                if (wind >= 8) System.out.print(" и сильный ветер");
                System.out.println(". Надевай майку и шорты.");
            }
            if (rain) System.out.println("Не забудь зонт. Там дождь.");
        } catch (InputMismatchException e) {
            System.out.print("Вы ввели не целое число.");
        }

    }
}